package src.config;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class SecurityFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String userNameAttribute = (String) request.getSession().getAttribute("login");
        String requestURI = request.getRequestURI();

        if (userNameAttribute != null ||
                requestURI.endsWith("login") ||
                requestURI.endsWith("login.jsp") ||
                requestURI.endsWith("css") ||
                requestURI.endsWith("png")
        ) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect(response.encodeRedirectURL("login.jsp"));
        }
    }

    @Override
    public void destroy() {

    }
}
