package src.controller;

import src.model.db.UserDB;
import src.model.User;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @EJB
    private UserDB userDB;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String password = req.getParameter("password");

        User userByLogin = userDB.findUser(login);

        //        userByLogin != null && password.equals(userByLogin.getPassword())
        //        login.equals("admin") && password.equals("admin")
        
        if (userByLogin != null && password.equals(userByLogin.getPassword())){
            req.getSession().setAttribute("login", login);
            resp.sendRedirect(resp.encodeRedirectURL("home.jsp"));
        }else{
            resp.sendRedirect(resp.encodeRedirectURL("login.jsp?invalidData"));
        }
    }
}
