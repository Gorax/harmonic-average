package src.controller.service;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class AverageService {

    private List<Integer> numberList = new ArrayList<>();

    public void addNumber(int number) {
        numberList.add(number);
    }

    public Double harmonicAverage() {
        int n = numberList.size();

        double denominator = 0.0;
        for (int i = 0; i < n; i++) {

            double number = numberList.get(i);
            denominator = denominator + (1 / number);
        }
        numberList.clear();
        return n / denominator;
    }

    public List<Integer> getNumberList() {
        return numberList;
    }
}
