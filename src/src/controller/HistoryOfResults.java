package src.controller;

import src.model.db.UserDB;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/historyOfResults")
public class HistoryOfResults extends HttpServlet {

    @EJB
    private UserDB userDB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Double> resultsList = userDB.findUser((String) req.getSession().getAttribute("login")).getListOfResults();

        if (!resultsList.isEmpty()){
            req.setAttribute("resultsList", resultsList);
            req.getRequestDispatcher("historyOfResults.jsp").forward(req,resp);
        }else{
            req.getRequestDispatcher("historyOfResults.jsp").forward(req,resp);
        }
    }
}
