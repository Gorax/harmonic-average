package src.controller;

import src.model.db.UserDB;
import src.controller.service.AverageService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/average")
public class NumberServlet extends HttpServlet {

    @EJB
    private AverageService averageService;

    @EJB
    private UserDB userDB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Double harmonicAverage = averageService.harmonicAverage();
        userDB.findUser((String) req.getSession().getAttribute("login")).getListOfResults().add(harmonicAverage);

        req.setAttribute("average", String.format("%.2f", harmonicAverage));
        req.getRequestDispatcher("average.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        averageService.addNumber(Integer.valueOf(req.getParameter("number")));

        req.setAttribute("numberList", averageService.getNumberList());
        req.getRequestDispatcher("average.jsp").forward(req, resp);

    }
}
