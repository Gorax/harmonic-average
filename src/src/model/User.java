package src.model;

import java.util.ArrayList;
import java.util.List;

public class User {

    String login;
    String password;
    List<Double> listOfResults;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
        this.listOfResults = new ArrayList();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List getListOfResults() {
        return listOfResults;
    }

    public void setListOfResults(List listOfResults) {
        this.listOfResults = listOfResults;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
