package src.model.db;

import src.model.User;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UserDB {

    List<User> userList = new ArrayList<>();

    @PostConstruct
    public void init(){
        userList.add(new User("admin", "admin"));
    }

    public User findUser(String login){
        User user = userList.stream().filter(e -> e.getLogin().equals(login)).findFirst().orElse(null);
        return user;
    }
}
